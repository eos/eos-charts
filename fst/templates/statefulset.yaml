apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ include "fst.fullname" . }}
  labels:
    {{- include "fst.labels" . | nindent 4 }}
spec:
  serviceName: {{ include "fst.fullname" . }}
  replicas: {{ .Values.replicaCount }}
  selector:
    matchLabels:
      {{- include "fst.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        {{- include "fst.selectorLabels" . | nindent 8 }}
        {{- toYaml .Values.customLabels | nindent 8 }}
    spec:
      hostNetwork: {{ default false .Values.hostNetwork }}
      dnsPolicy: {{ default "ClusterFirst" .Values.dnsPolicy }}
      {{- if .Values.podAssignment.enableNodeSelector }}
      nodeSelector:
        {{- toYaml .Values.customLabels | nindent 8 }}
      {{- end }}
      {{- if .Values.podAssignment.enablePodAntiAffinity }}
      affinity:
        podAntiAffinity:
          preferredDuringSchedulingIgnoredDuringExecution:
          - weight: 50
            podAffinityTerm:
              labelSelector:
                matchExpressions:
                {{- range $key, $val := .Values.customLabels }}
                - key: {{ $key }}
                  operator: In
                  values:
                  - {{ $val }}
                {{- end }}
              topologyKey: "kubernetes.io/hostname"
      {{- end }}
      {{- if .Values.priorityClassName }}
      priorityClassName: {{ .Values.priorityClassName }}
      {{- end }}
      securityContext:
        seccompProfile:
          type: RuntimeDefault
      {{- if .Values.pod.extraSpec }}
      {{ .Values.pod.extraSpec | toYaml | nindent 6 }}
      {{- end }}
      initContainers:
        # Required until https://github.com/kubernetes/kubernetes/issues/81089 is merged
        - name: eos-fst-init0-sss-keytab-ownership
          image: {{ include "utils.image" . }}
          imagePullPolicy: {{ include "utils.imagePullPolicy" . }}
          command: ["/bin/bash", "-c"]
          args: ["cp /root/sss_keytab/input/eos.keytab /root/sss_keytab/output/eos.keytab; chown daemon:daemon /root/sss_keytab/output/eos.keytab; chmod 400 /root/sss_keytab/output/eos.keytab"]
          securityContext:
            allowPrivilegeEscalation: {{ include "utils.securityContext.allowPrivilegeEscalation" . }}
          volumeMounts:
            - name: eos-sss-keytab
              mountPath: /root/sss_keytab/input/eos.keytab
              subPath: eos.keytab
            - name: eos-sss-keytab-fixedownership
              mountPath: /root/sss_keytab/output
        - name: eos-fst-init1-chown-storage
          image: {{ include "utils.image" . }}
          imagePullPolicy: {{ include "utils.imagePullPolicy" . }}
          command: ["/bin/sh", "-c"]
          args: ["chown daemon:daemon {{ .Values.storageMountPath }}"]
          securityContext:
            allowPrivilegeEscalation: {{ include "utils.securityContext.allowPrivilegeEscalation" . }}
          volumeMounts:
            - name: fst-storage
              mountPath: {{ .Values.storageMountPath }}
        {{- if .Values.selfRegister }}
        {{- if .Values.selfRegister.enable }}
        - name: eos-fst-init2-fst-init
          image: {{ include "utils.image" . }}
          imagePullPolicy: {{ include "utils.imagePullPolicy" . }}
          env:
            - name: EOS_MGM_URL
              value: root://{{ include "utils.mgm_fqdn" . }}
          envFrom:
            - configMapRef:
                name: {{ include "fst.fullname" . }}-cfgmap-sysconfig-eos
          securityContext:
            privileged: {{ include "utils.securityContext.privileged" . }}
            allowPrivilegeEscalation: {{ include "utils.securityContext.allowPrivilegeEscalation" . }}
            capabilities:
              add:
              - SYS_PTRACE
          command: ["/bin/bash", "/root/fst_init.sh"]
          volumeMounts:
            - name: fst-cfgmap-xrd-cf-fst
              mountPath: /etc/xrd.cf.fst
              subPath: xrd.cf.fst
            - name: eos-sss-keytab-fixedownership
              mountPath: /etc/eos.keytab
              subPath: eos.keytab
            - name: fst-storage
              mountPath: {{ .Values.storageMountPath }}
            - name: fst-cfgmap-fst-init
              mountPath: /root/fst_init.sh
              subPath: fst_init.sh
        {{- end }}
        {{- end }}
      {{- if and .Values.initContainer.enabled .Values.initContainer.script }}
        - name: eos-fst-init99-custom-initscript
          image: {{ include "utils.image" . }}
          imagePullPolicy: {{ include "utils.imagePullPolicy" . }}
          command: ["/bin/bash", "/initconfigmap/initscript.sh"]
          securityContext:
            allowPrivilegeEscalation: {{ include "utils.securityContext.allowPrivilegeEscalation" . }}
          volumeMounts:
            - name: fst-initscript-configmap
              mountPath: /initconfigmap
            {{- if .Values.initContainer.volumeMounts }}
            {{ .Values.initContainer.volumeMounts | toYaml | nindent 12 }}
            {{- end }}
          {{- if .Values.initContainer.spec }}
          {{ .Values.initContainer.spec | toYaml | nindent 10 }}
          {{- end }}
      {{- end }}
      containers:
        - name: eos-fst
          image: {{ include "utils.image" . }}
          imagePullPolicy: {{ include "utils.imagePullPolicy" . }}
          command: ["/bin/sh", "-c"]
          args: ["/opt/eos/xrootd/bin/xrootd -n fst -c /etc/xrd.cf.fst -Rdaemon"]
          env:
            - name: EOS_MGM_URL
              value: root://{{ include "utils.mgm_fqdn" . }}
            - name: EOS_FS_FULL_SIZE_IN_GB
              value: {{ default 5 .Values.minFsSizeGb | quote }}
            {{- with .Values.extraEnv }}
            {{- include "utils.extraEnv" . | nindent 12 }}
            {{- end }}
          envFrom:
            - configMapRef:
                name: {{ include "fst.fullname" . }}-cfgmap-sysconfig-eos
          {{- if $.Values.resources }}
          resources:
            {{-  toYaml $.Values.resources | nindent 12 }}
          {{- end }}
          securityContext:
            privileged: {{ include "utils.securityContext.privileged" . }}
            allowPrivilegeEscalation: {{ include "utils.securityContext.allowPrivilegeEscalation" . }}
            capabilities:
              add:
              - SYS_PTRACE
          {{- include "fst.livenessProbe" . | nindent 10 }}
          volumeMounts:
            - name: fst-cfgmap-xrd-cf-fst
              mountPath: /etc/xrd.cf.fst
              subPath: xrd.cf.fst
            ## TODO: What about the data at /var/eos/md/... ?
            ##   Should this be persisted?
            ## - name: fst-data
            ##   mountPath: /var/eos
            - name: eos-sss-keytab-fixedownership
              mountPath: /etc/eos.keytab
              subPath: eos.keytab
            - name: fst-storage
              mountPath: {{ .Values.storageMountPath }}
            {{- if .Values.extraVolumes.volumeMounts }}
            {{- .Values.extraVolumes.volumeMounts | toYaml | nindent 12 }}
            {{- end }}
      volumes:
        - name: fst-cfgmap-xrd-cf-fst
          configMap:
            name: {{ include "fst.fullname" . }}-cfgmap-xrd-cf-fst
            defaultMode: 0755
        - name: fst-cfgmap-fst-init
          configMap:
            name: {{ include "fst.fullname" . }}-cfgmap-fst-init
            defaultMode: 0755
        - name: eos-sss-keytab
          secret:
            secretName: {{ include "utils.sssKeytabName" . }}
            defaultMode: 0400
        - name: eos-sss-keytab-fixedownership
          emptyDir: {}
        {{- if .Values.extraVolumes.volumes }}
        {{- .Values.extraVolumes.volumes | toYaml | nindent 8 }}
        {{- end }}
        {{- if .Values.initContainer.script }}
        - name: fst-initscript-configmap
          configMap:
            name: {{ include "fst.fullname" . }}-cfgmap-fst-initscript
            defaultMode: 0755
        {{- end }}
  # For persistent volumes, we have 2 alternatives
  # 1. Persistence is disabled: use emptyDir
  # 2. Persistence is enable and no existingClaim: set the claim
  {{- if eq (include "utils.persistence" . ) "false" }}
        - name: fst-storage
          emptyDir: {}
  {{- else }}
    {{- if not .Values.persistence.volumeClaimTemplates }}
        - name: fst-storage
          persistentVolumeClaim:
            claimName: {{ include "fst.fullname" . }}-pvc
    {{- else }}
  volumeClaimTemplates:
    - metadata:
        name: fst-storage
        {{- if .Values.persistence.annotations }}
        annotations:
          {{- toYaml .Values.persistence.annotations | nindent 10 }}
        {{- end }}
      spec:
        {{- if .Values.persistence.storageClass }}
        {{- if (eq "-" .Values.persistence.storageClass) }}
        storageClassName: ""
        {{- else }}
        storageClassName: "{{ .Values.persistence.pvc.storageClass }}"
        {{- end }}
        {{- end }}
        accessModes:
          {{- range .Values.persistence.accessModes }}
          - {{ . | quote }}
          {{- end }}
        resources:
          requests:
            storage: {{ .Values.persistence.size | quote }}
    {{- end }}
  {{- end }}
