apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ include "mgm.fullname" . }}
  labels:
    {{- include "mgm.labels" . | nindent 4 }}
spec:
  serviceName: {{ include "mgm.fullname" . }}
  replicas: {{ .Values.replicaCount }}
  selector:
    matchLabels:
      {{- include "mgm.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        {{- include "mgm.selectorLabels" . | nindent 8 }}
        {{- toYaml .Values.customLabels | nindent 8 }}
    spec:
      {{- if .Values.priorityClassName }}
      priorityClassName: {{ .Values.priorityClassName }}
      {{- end }}
      securityContext:
        fsGroup: 2
        seccompProfile:
          type: RuntimeDefault
      hostNetwork: {{ default false .Values.hostNetwork }}
      dnsPolicy: {{ default "ClusterFirst" .Values.dnsPolicy }}
      {{- if .Values.podAssignment.enableNodeSelector }}
      nodeSelector:
        {{- toYaml .Values.customLabels | nindent 8 }}
      {{- end }}
      {{- if .Values.pod.extraSpec }}
      {{ .Values.pod.extraSpec | toYaml | nindent 6 }}
      {{- end }}
      initContainers:
        - name: eos-mgm-init0
          image: {{ include "utils.image" . }}
          imagePullPolicy: {{ include "utils.imagePullPolicy" . }}
          command: ["/usr/bin/bash", "-c"]
          # Copying and chowning the keytab file can be avoided if https://github.com/kubernetes/kubernetes/issues/81089 is addressed.
          args: ["mkdir -v -p /var/log/eos/mgm /var/log/eos/tx /var/eos/ns-queue;
                 cp -v /input/eos.keytab /output/eos.keytab;
                 chown -v daemon:daemon /output/eos.keytab;
                 chmod -v 400 /output/eos.keytab"]
          securityContext:
            runAsUser: 2
            allowPrivilegeEscalation: {{ include "utils.securityContext.allowPrivilegeEscalation" . }}
          volumeMounts:
            - name: mgm-logs
              mountPath: /var/log/eos
            - name: mgm-data
              mountPath: /var/eos
            - name: eos-sss-keytab
              mountPath: /input/eos.keytab
              subPath: eos.keytab
            - name: eos-sss-keytab-fixedownership
              mountPath: /output
      {{- if and .Values.initContainer.enabled .Values.initContainer.script }}
        - name: eos-mgm-init1-custom-initscript
          image: {{ include "utils.image" . }}
          imagePullPolicy: {{ include "utils.imagePullPolicy" . }}
          command: ["/usr/bin/bash", "/initconfigmap/initscript.sh"]
          securityContext:
            allowPrivilegeEscalation: {{ include "utils.securityContext.allowPrivilegeEscalation" . }}
          volumeMounts:
            - name: mgm-initscript-configmap
              mountPath: /initconfigmap
            {{- if .Values.initContainer.volumeMounts }}
            {{ .Values.initContainer.volumeMounts | toYaml | nindent 12 }}
            {{- end }}
          {{- if .Values.initContainer.spec }}
          {{ .Values.initContainer.spec | toYaml | nindent 10 }}
          {{- end }}
      {{- end }}
      {{- if .Values.kerberos.enabled }}
        - name: eos-mgm-init2-get-krb5-keytab
          image: {{ include "utils.image" . }}
          imagePullPolicy: {{ include "utils.imagePullPolicy" . }}
          command: ["/bin/bash", "-c"]
          args: ["
            sukadmin() { echo {{ .Values.kerberos.adminPrinc.password }} | kadmin -r {{ .Values.kerberos.defaultRealm | upper }} -p {{ .Values.kerberos.adminPrinc.name }}/admin $@; };
            sukadmin addprinc -pw $(hostname) host/$(hostname -f);
            sukadmin ktadd -k /root/krb5_keytab/output/eos.krb5.keytab host/$(hostname -f);
            chown daemon:daemon /root/krb5_keytab/output/eos.krb5.keytab"]
          securityContext:
            allowPrivilegeEscalation: {{ include "utils.securityContext.allowPrivilegeEscalation" . }}
          volumeMounts:
            - name: eos-krb5-conf
              mountPath: /etc/krb5.conf
              subPath: krb5.conf
            - name: eos-krb5-keytab
              mountPath: /root/krb5_keytab/output
      {{- end }}
        - name: eos-mgm-init3-mgm-init
          image: {{ include "utils.image" . }}
          imagePullPolicy: {{ include "utils.imagePullPolicy" . }}
          envFrom:
            - configMapRef:
                name: {{ include "mgm.fullname" . }}-cfgmap-sysconfig-eos
          securityContext:
            privileged: {{ include "utils.securityContext.privileged" . }}
            allowPrivilegeEscalation: {{ include "utils.securityContext.allowPrivilegeEscalation" . }}
            capabilities:
              add:
              - SYS_PTRACE
          command: ["/usr/bin/bash", "-c"]
          args: ["{{- if .Values.preRun.enabled }}/etc/eos.configmap/mgm_pre_run.sh; {{- end }}/etc/eos.configmap/mgm_init.sh"]
          volumeMounts:
            - name: mgm-cfgmap-xrd-cf-mgm
              mountPath: /etc/xrd.cf.mgm
              subPath: xrd.cf.mgm
            - name: eos-sss-keytab-fixedownership
              mountPath: /etc/eos.keytab
              subPath: eos.keytab
            {{- if eq (include "utils.httpAccess.enabled" .) "true" }}
            - name: eos-mgm-secrets
              mountPath: /etc/eos.secrets
            {{- end }}
            {{- if .Values.http.sciTokens.enabled }}
            - name: eos-mgm-scitokens
              mountPath: /etc/xrootd/scitokens.cfg
              subPath: scitokens.cfg
            {{- end }}
            {{- if .Values.kerberos.enabled }}
            - name: eos-krb5-conf
              mountPath: /etc/krb5.conf
              subPath: krb5.conf
            - name: eos-krb5-keytab
              mountPath: /etc/eos.krb5.keytab
              subPath: eos.krb5.keytab
            {{- end }}
            - name: mgm-data
              mountPath: /var/eos
            - name: mgm-logs
              mountPath: /var/log/eos
            - name: mgm-cfgmap-mgm-init
              mountPath: /etc/eos.configmap/
            {{- if .Values.extraVolumes.volumeMounts }}
            {{- .Values.extraVolumes.volumeMounts | toYaml | nindent 12 }}
            {{- end }}
      containers:
        - name: eos-mgm
          image: {{ include "utils.image" . }}
          imagePullPolicy: {{ include "utils.imagePullPolicy" . }}
          command: ["/usr/bin/bash", "-c"]
          args: ["{{- if .Values.preRun.enabled }}/etc/eos.configmap/mgm_pre_run.sh; {{- end }}/opt/eos/xrootd/bin/xrootd -n mgm -c /etc/xrd.cf.mgm -l /var/log/eos/xrdlog.mgm -Rdaemon"]
          env:
            - name: LD_PRELOAD
              value: "/usr/lib64/libjemalloc.so"
            {{- with .Values.extraEnv }}
            {{- include "utils.extraEnv" . | nindent 12 }}
            {{- end }}
          envFrom:
            - configMapRef:
                name: {{ include "mgm.fullname" . }}-cfgmap-sysconfig-eos
          {{- if $.Values.resources }}
          resources:
            {{-  toYaml $.Values.resources | nindent 12 }}
          {{- end }}
          securityContext:
            privileged: {{ include "utils.securityContext.privileged" . }}
            allowPrivilegeEscalation: {{ include "utils.securityContext.allowPrivilegeEscalation" . }}
            capabilities:
              add:
              - SYS_PTRACE
          {{- include "mgm.startupProbe" . | nindent 10 }}
          {{- include "mgm.livenessProbe" . | nindent 10 }}
          {{- include "mgm.readinessProbe" . | nindent 10 }}
          volumeMounts:
            {{- if .Values.ldapBindUsers.enable }}
            - name: nscd-socket
              mountPath: /run/nscd
            - name: cfgmap-nsswitch-conf
              mountPath: /etc/nsswitch.conf
              subPath: nsswitch.conf
            {{- end }}
            - name: mgm-cfgmap-xrd-cf-mgm
              mountPath: /etc/xrd.cf.mgm
              subPath: xrd.cf.mgm
            - name: eos-sss-keytab-fixedownership
              mountPath: /etc/eos.keytab
              subPath: eos.keytab
            {{- if .Values.preRun.enabled }}
            - name: mgm-cfgmap-mgm-init
              mountPath: /etc/eos.configmap/mgm_pre_run.sh
              subPath: mgm_pre_run.sh
            {{- end }}
            {{- if eq (include "utils.httpAccess.enabled" .) "true" }}
            - name: eos-mgm-secrets
              mountPath: /etc/eos.secrets
            {{- end }}
            {{- if .Values.http.sciTokens.enabled }}
            - name: eos-mgm-scitokens
              mountPath: /etc/xrootd/scitokens.cfg
              subPath: scitokens.cfg
            {{- end }}
            {{- if .Values.kerberos.enabled }}
            - name: eos-krb5-conf
              mountPath: /etc/krb5.conf
              subPath: krb5.conf
            - name: eos-krb5-keytab
              mountPath: /etc/eos.krb5.keytab
              subPath: eos.krb5.keytab
            {{- end }}
            - name: mgm-data
              mountPath: /var/eos
            - name: mgm-logs
              mountPath: /var/log/eos
            {{- if .Values.extraVolumes.volumeMounts }}
            {{- .Values.extraVolumes.volumeMounts | toYaml | nindent 12 }}
            {{- end }}
        {{- if .Values.global.enableMQ }}
        - name: eos-mq
          image: {{ include "utils.image" . }}
          imagePullPolicy: {{ include "utils.imagePullPolicy" . }}
          command: ["/bin/sh", "-c"]
          args: ["/opt/eos/xrootd/bin/xrootd -n mq -c /etc/xrd.cf.mq -l /var/log/eos/xrdlog.mq -Rdaemon"]
          env:
            {{- with .Values.extraEnv }}
            {{- include "utils.extraEnv" . | nindent 12 }}
            {{- end }}
          envFrom:
            - configMapRef:
                name: {{ include "mq.fullname" . }}-cfgmap-sysconfig-eos
          securityContext:
            privileged: {{ include "utils.securityContext.privileged" . }}
            allowPrivilegeEscalation: {{ include "utils.securityContext.allowPrivilegeEscalation" . }}
            capabilities:
              add:
              - SYS_PTRACE
          {{- include "mq.livenessProbe" . | nindent 10 }}
          volumeMounts:
            - name: mq-cfgmap-xrd-cf-mq
              mountPath: /etc/xrd.cf.mq
              subPath: xrd.cf.mq
            - name: eos-sss-keytab-fixedownership
              mountPath: /etc/eos.keytab
              subPath: eos.keytab
            - name: mq-logs
              mountPath: /var/log/eos
        {{- end }}
        {{- if .Values.ldapBindUsers.enable }}
        - name: nslcd
          {{- with .Values.ldapBindUsers.nslcd.image }}
          image: "{{ .repository }}:{{ .tag }}"
          imagePullPolicy: {{ default "Always" .pullPolicy }}
          {{- end }}
          securityContext:
            allowPrivilegeEscalation: {{ include "utils.securityContext.allowPrivilegeEscalation" . }}
          volumeMounts:
            - name: nslcd-cfgmap-nslcd-conf
              mountPath: /etc/nslcd.conf
              subPath: nslcd.conf
            - name: nslcd-socket
              mountPath: /run/nslcd
        - name: nscd
          {{- with .Values.ldapBindUsers.nscd.image }}
          image: "{{ .repository }}:{{ .tag }}"
          imagePullPolicy: {{ default "Always" .pullPolicy }}
          {{- end }}
          securityContext:
            allowPrivilegeEscalation: {{ include "utils.securityContext.allowPrivilegeEscalation" . }}
          volumeMounts:
            - name: cfgmap-nsswitch-conf
              mountPath: /etc/nsswitch.conf
              subPath: nsswitch.conf
            - name: nslcd-socket
              mountPath: /run/nslcd
            - name: nscd-socket
              mountPath: /run/nscd
        {{- end }}
      volumes:
        {{- if .Values.ldapBindUsers.enable }}
        - name: nslcd-cfgmap-nslcd-conf
          configMap:
            name: {{ include "mgm.fullname" . }}-cfgmap-nslcd-conf
            defaultMode: 0600
        - name: cfgmap-nsswitch-conf
          configMap:
            name: {{ include "mgm.fullname" . }}-cfgmap-nsswitch-conf
            defaultMode: 0600
        - name: nslcd-socket
          emptyDir:
            medium: Memory
        - name: nscd-socket
          emptyDir:
            medium: Memory
        {{- end }}
        - name: mgm-cfgmap-xrd-cf-mgm
          configMap:
            name: {{ include "mgm.fullname" . }}-cfgmap-xrd-cf-mgm
            defaultMode: 0755
        - name: mgm-cfgmap-mgm-init
          configMap:
            name: {{ include "mgm.fullname" . }}-cfgmap-mgm-init
            defaultMode: 0755
        - name: eos-sss-keytab
          secret:
            secretName: {{ include "utils.sssKeytabName" . }}
            defaultMode: 0400
        - name: eos-sss-keytab-fixedownership
          emptyDir: {}
        {{- if eq (include "utils.httpAccess.enabled" .) "true" }}
        - name: eos-mgm-secrets
          secret:
            secretName: {{ include "mgm.fullname" . }}-secrets
            defaultMode: 0400
        {{- end }}
        {{- if .Values.http.sciTokens.enabled }}
        - name: eos-mgm-scitokens
          configMap:
            name: {{ include "mgm.fullname" . }}-scitokens.cfg
            items:
            - key: scitokens.cfg
              path: scitokens.cfg
        {{- end }}
        {{- if .Values.global.enableMQ }}
        - name: mq-cfgmap-xrd-cf-mq
          configMap:
            name: {{ include "mq.fullname" . }}-cfgmap-xrd-cf-mq
            defaultMode: 0644
        - name: mq-logs
          emptyDir: {}
        {{- end }}
        {{- if .Values.kerberos.enabled }}
        - name: eos-krb5-conf
          configMap:
            name: {{ include "mgm.krb5ConfConfigMapName" . }}
            defaultMode: 0644
        - name: eos-krb5-keytab
          emptyDir: {}
        {{- end }}
        - name: mgm-logs
          emptyDir: {}
        {{- if .Values.extraVolumes.volumes }}
        {{- .Values.extraVolumes.volumes | toYaml | nindent 8 }}
        {{- end }}
        {{- if .Values.initContainer.script }}
        - name: mgm-initscript-configmap
          configMap:
            name: {{ include "mgm.fullname" . }}-cfgmap-mgm-initscript
            defaultMode: 0755
        {{- end }}
  # For persistent volumes, we have 2 alternatives
  # 1. Persistence is disabled: use emptyDir
  # 2. Persistence is enable and no existingClaim: set the claim
  {{- if eq (include "utils.persistence" . ) "false" }}
        - name: mgm-data
          emptyDir: {}
  {{- else }}
  volumeClaimTemplates:
    - metadata:
        name: mgm-data
        {{- if .Values.persistence.annotations }}
        annotations:
          {{- toYaml .Values.persistence.annotations | nindent 10 }}
        {{- end }}
      spec:
        {{- if .Values.persistence.storageClass }}
        {{- if (eq "-" .Values.persistence.storageClass) }}
        storageClassName: ""
        {{- else }}
        storageClassName: "{{ .Values.persistence.pvc.storageClass }}"
        {{- end }}
        {{- end }}
        accessModes:
          {{- range .Values.persistence.accessModes }}
          - {{ . | quote }}
          {{- end }}
        resources:
          requests:
            storage: {{ .Values.persistence.size | quote }}
  {{- end }}
